from os.path import dirname, join

import mosaik
import pytest

from .support import compare_influxdb


sim_config = {
    'Sim': {'python': 'example_sim.mosaik:ExampleSim'},
     'influx': {
            'python': 'mosaik_influxdb:MosaikInfluxDB',
        },
}


@pytest.yield_fixture
def world():
    world = mosaik.World(sim_config)
    yield world
    if world.srv_sock:
        world.shutdown()


def test_mosaik_influxdb(world, tmpdir):
    duration = 2
    sim_a = world.start('Sim')
    sim_b = world.start('Sim')
    a = sim_a.A(init_val=0)
    b = sim_b.B.create(2, init_val=0)
    for e in b:
        world.connect(a, e, ('val_out', 'val_in'))

    # influxdb
    influx = world.start('influx', step_size=1)
    influx_db = influx.Database(influxDBHost='localhost', influxDBPort='8086', influxDBUser='', influxDBPass='',
                         influxDBName='influx_Unit_Test',influxTableName='MostafaTable')


    for e in [a] + b:
        world.connect(e, influx_db, 'val_out')



    world.run(until=duration)
    assert compare_influxdb()


